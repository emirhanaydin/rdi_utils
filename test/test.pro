TEMPLATE = app
TARGET = test
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++17
QMAKE_CXXFLAGS += -fopenmp
SOURCES += main.cpp \
    tests.cpp \
    diff_tests.cpp


CONFIG(release, debug|release) {
    LIBS += -L$$/opt/rdi/lib
}
CONFIG(debug, debug|release) {
    LIBS += -L$$/opt/rdi/lib_debug
}

LIBS += -L$$OUT_PWD/../lib
LIBS += -pthread -fopenmp -lrdi_utils

INCLUDEPATH += /opt/rdi/include
INCLUDEPATH += ../lib

DEFINES += CODE_LOCATION="\\\"$$PWD/\\\""


CONFIG(release, debug|release){
#  DEFINES += RDI_CONSOLE_LOGGING
#  DEFINES += RDI_FILE_LOGGING
}
CONFIG(debug, debug|release) {
  DEFINES += RDI_CONSOLE_LOGGING
  DEFINES += RDI_FILE_LOGGING
 }

HEADERS += \
    rdi_dirty_buckwalter.h
