#include <catch.hpp>

#include <vector>
#include <string>

#include "rdi_diff.hpp"

using namespace std;
using namespace RDI;

static const vector<string> atary1 =
{
"SENT-START",
"CW_C_m_",
"CW_V_m_alefmad2_tabee",
"CW_C_l_",
"CW_V_l_kasra",
"CW_C_k_",
"CW_V_k_kasra",
"CW_C_y_",
"CW_C_w_",
"TE_V_0867_w_fatha",
"CW_C_m_",
"CW_V_m_kasra",
"RE_C_0365_@_",
"RE_V_0365_@_fatha",
"RE_C_0146_l_",
"EC_C_0147_d_",
"CW_V_d_yaamad4_ared",
"CW_C_n_",
"SENT-END"
};

static const vector<string> atary2 =
{
"SENT-START",
"CW_C_m_",
"CW_V_m_alefmad2_tabee",
"CW_C_l_",
"CW_V_l_kasra",
"CW_C_k_",
"CW_V_k_kasra",
"CW_C_y_",
"CW_V_y_fatha",
"CW_C_w_",
"CW_C_m_",
"CW_V_m_kasra",
"CW_C_d_shadda_",
"CW_V_d_yaamad4_ared",
"CW_C_n_",
"SENT-END"
};

static const vector<string> playstation1 =
{
"SENT-START",
"CW_C_S_",
"CW_V_S_kasra",
"RE_C_0379_r_",
"CW_V_R_alefmad2_tabee",
"RE_C_0097_t_",
"CW_V_T_fatha",
"CW_C_l_shadda_",
"TE_V_0823_l_kasra",
"CW_C_~z_",
"CW_V_~z_yaamad2_tabee",
"CW_C_n_",
"TE_V_0843_n_kasra",
"CW_C_@_",
"TE_V_0603_@_kasra",
"CW_C_n_",
"CW_C_~@_",
"CW_V_~@_fatha",
"CW_C_m_",
"CW_C_t_",
"CW_V_t_fatha",
"CW_C_~@_",
"CW_V_~@_fatha",
"CW_C_l_",
"CW_V_l_fatha",
"CW_C_y_",
"CW_C_h_",
"CW_V_h_kasra",
"CW_C_m_",
"CW_C_g_h_",
"CW_V_g_h_fatha",
"CW_C_y_",
"CW_C_r_",
"CW_V_r_kasra",
"CW_C_l_",
"CW_C_m_",
"CW_V_m_fatha",
"CW_C_g_h_",
"TE_V_0789_g_h_damma",
"RE_C_0194_d_",
"CW_V_D_wawmad2_tabee",
"CW_C_b_",
"RE_V_0036_b_fatha",
"CW_C_~@_",
"CW_V_~@_fatha",
"CW_C_l_",
"CW_V_l_fatha",
"CW_C_y_",
"CW_C_h_",
"CW_V_h_kasra",
"CW_C_m_",
"CW_C_w_",
"CW_V_w_fatha",
"CW_C_l_",
"RE_V_0119_L_fatha",
"RE_C_0381_d__shadda_",
"CW_V_D_alefmad6_lazem",
"CW_C_l_shadda_",
"CW_V_l_yaamad4_ared",
"CW_C_n_",
"SENT-END"
};

static const vector<string> playstation2 =
{
"SENT-START",
"CW_C_S_",
"CW_V_S_kasra",
"CW_C_R_",
"CW_V_R_alefmad2_tabee",
"CW_C_T_",
"CW_V_T_fatha",
"CW_C_l_shadda_",
"CW_V_l_fatha",
"CW_C_~z_",
"CW_V_~z_yaamad2_tabee",
"CW_C_n_",
"CW_V_n_fatha",
"CW_C_@_",
"CW_V_@_fatha",
"CW_C_n_",
"CW_C_~@_",
"CW_V_~@_fatha",
"CW_C_m_",
"CW_C_t_",
"CW_V_t_fatha",
"CW_C_~@_",
"CW_V_~@_fatha",
"CW_C_l_",
"CW_V_l_fatha",
"CW_C_y_",
"CW_C_h_",
"CW_V_h_kasra",
"CW_C_m_",
"CW_C_g_h_",
"CW_V_g_h_fatha",
"CW_C_y_",
"CW_C_r_",
"CW_V_r_kasra",
"CW_C_l_",
"CW_C_m_",
"CW_V_m_fatha",
"CW_C_g_h_",
"CW_C_D_",
"CW_V_D_wawmad2_tabee",
"CW_C_b_",
"CW_V_b_kasra",
"CW_C_~@_",
"CW_V_~@_fatha",
"CW_C_l_",
"CW_V_l_fatha",
"CW_C_y_",
"CW_C_h_",
"CW_V_h_kasra",
"CW_C_m_",
"CW_C_w_",
"CW_V_w_fatha",
"CW_C_l_",
"CW_V_l_fatha",
"CW_C_D_shadda_",
"CW_V_D_alefmad6_lazem",
"CW_C_l_shadda_",
"CW_V_l_yaamad4_ared",
"CW_C_n_",
"SENT-END"
};

static const vector<string> xbox1 =
{
"b",
"i",
"s",
"m",
"i",
"l<c>l",
"m_i",
"h",
"i",
"R<c>R",
"A",
"~h",
"m",
"m_a",
"n",
"i",
"R<c>R",
"A",
"~h",
"m_i",
"n"
};

static const vector<string> xbox2 =
{
"b",
"i",
"s",
"*",
"m",
"i",
"*",
"*",
"*",
"l<c>l",
"m_a",
"h",
"i",
"*",
"*",
"*",
"R<c>R",
"A",
"~h",
"*",
"m",
"m_a",
"n",
"i",
"*",
"*",
"*",
"R<c>R",
"A",
"~h",
"m_i",
"m",
"*"
};

bool
operator==(const Instruction& i1, const Instruction& i2)
{
	return i1.type == i2.type &&
		   i1.what == i2.what &&
		   i1.where.begin == i2.where.begin &&
		   i1.where.end == i2.where.end;
}

TEST_CASE("Diff", "[diff]")
{
	vector<Instruction> actual_patch = diff(atary1, atary2);

	vector<Instruction> expected_patch =
	{
		{
			{"CW_V_y_fatha"},
			1,
			{7, 7},
			Instruction::Insert
		},
		{
			{},
			2,
			{9, 9},
			Instruction::Delete
		},
		{
			{},
			4,
			{12, 14},
			Instruction::Delete
		},
		{
			{"CW_C_d_shadda_"},
			13,
			{15, 15},
			Instruction::Substitute
		}
	};

	REQUIRE(actual_patch.size() == expected_patch.size());

	for(size_t i = 0; i < actual_patch.size(); i++)
	{
		CHECK(actual_patch[i] == expected_patch[i]);
	}
}

TEST_CASE("Patch", "[diff]")
{
	vector<Instruction> mypatch =
	{
		{
			{"CW_V_y_fatha"},
			1,
			{7, 7},
			Instruction::Insert
		},
		{
			{},
			2,
			{9, 9},
			Instruction::Delete
		},
		{
			{},
			4,
			{12, 14},
			Instruction::Delete
		},
		{
			{"CW_C_d_shadda_"},
			13,
			{15, 15},
			Instruction::Substitute
		}
	};

	vector<string> actual = patch(atary1, mypatch);

	CHECK(actual == atary2);
}

TEST_CASE("Diff and Patch", "[diff]")
{
	SECTION("Playstation")
	{
		vector<Instruction> p = diff(playstation1, playstation2);
		vector<string> fake_playstation2 = patch(playstation1, p);
		CHECK(fake_playstation2 == playstation2);
	}
	SECTION("Xbox")
	{
		vector<Instruction> p = diff(xbox1, xbox2);
		vector<string> fake_xbox2 = patch(xbox1, p);
		CHECK(fake_xbox2 == xbox2);
	}
}

TEST_CASE("One is the subset of the other", "[diff]")
{
	SECTION("1st is subset of 2nd")
	{
		vector<string> vec1 = {"a", "b"};
		vector<string> vec2 = {"a", "b", "c", "d"};
		vector<Instruction> p = diff(vec1, vec2);
		CHECK(patch(vec1, p) == vec2);
	}
	SECTION("2nd is subset of 1st")
	{
		vector<string> vec1 = {"a", "b", "c", "d"};
		vector<string> vec2 = {"a", "b"};
		vector<Instruction> p = diff(vec1, vec2);
		CHECK(patch(vec1, p) == vec2);
	}
	SECTION("only one element")
	{
		vector<string> vec1 = {"a"};
		vector<string> vec2 = {"a", "b", "c", "d"};
		vector<Instruction> p = diff(vec1, vec2);
		CHECK(patch(vec1, p) == vec2);
	}
}

TEST_CASE("Collapse Substitutions", "[diff]")
{
	SECTION("more insertions than deletions")
	{
		const vector<string> v1 = {"a", "b", "c", "d"};
		const vector<string> v2 = {"x", "y", "z", "l", "m"};
		vector<Instruction> actual = diff(v1, v2);

		vector<Instruction> expected =
		{
			{
				{"x", "y", "z", "l"},
				0,
				{0, 3},
				Instruction::Substitute
			},
			{
				{"m"},
				4,
				{3, 3},
				Instruction::Insert
			}
		};

		REQUIRE(actual.size() == expected.size());

		for(size_t i = 0; i < actual.size(); i++)
		{
			CHECK(actual[i] == expected[i]);
		}

		vector<string> x = patch(v1, actual);

		REQUIRE(x == v2);
	}

	SECTION("more deletions than insertions")
	{
		const vector<string> v1 = {"a", "b", "c", "d"};
		const vector<string> v2 = {"x", "y", "z"};
		vector<Instruction> actual = diff(v1, v2);

		vector<Instruction> expected =
		{
			{
				{},
				0,
				{0, 0},
				Instruction::Delete
			},
			{
				{"x", "y", "z"},
				1,
				{1, 3},
				Instruction::Substitute
			}
		};

		REQUIRE(actual.size() == expected.size());

		for(size_t i = 0; i < actual.size(); i++)
		{
			CHECK(actual[i] == expected[i]);
		}

		vector<string> x = patch(v1, actual);

		REQUIRE(x == v2);
	}

	SECTION("equal deletions and insertions")
	{
		const vector<string> v1 = {"a", "b", "c"};
		const vector<string> v2 = {"x", "y", "z"};
		vector<Instruction> actual = diff(v1, v2);

		vector<Instruction> expected =
		{
			{
				{"x", "y", "z"},
				0,
				{0, 2},
				Instruction::Substitute
			}
		};

		REQUIRE(actual.size() == expected.size());

		for(size_t i = 0; i < actual.size(); i++)
		{
			CHECK(actual[i] == expected[i]);
		}

		vector<string> x = patch(v1, actual);

		REQUIRE(x == v2);
	}
}

TEST_CASE("overlapping prefix/postfix", "[diff]")
{
	SECTION("case 1")
	{
		const vector<string> v1 = {"a", "b", "c"};
		const vector<string> v2 = {"a", "b", "e", "b", "c"};
		vector<Instruction> actual = diff(v1, v2);

		vector<Instruction> expected =
		{
			{
				{"e", "b"},
				0,
				{1, 1},
				Instruction::Insert
			}
		};

		REQUIRE(actual.size() == expected.size());

		for(size_t i = 0; i < actual.size(); i++)
		{
			CHECK(actual[i] == expected[i]);
		}

		vector<string> x = patch(v1, actual);

		REQUIRE(x == v2);
	}
	SECTION("case 2")
	{
		const vector<string> v1 = {"a", "b", "e", "b", "c"};
		const vector<string> v2 = {"a", "b", "c"};
		vector<Instruction> actual = diff(v1, v2);

		vector<Instruction> expected =
		{
			{
				{},
				0,
				{2, 3},
				Instruction::Delete
			}
		};

		REQUIRE(actual.size() == expected.size());

		for(size_t i = 0; i < actual.size(); i++)
		{
			CHECK(actual[i] == expected[i]);
		}

		vector<string> x = patch(v1, actual);

		REQUIRE(x == v2);
	}
}
