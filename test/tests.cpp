#include <catch.hpp>

#include <vector>
#include <string>
#include <list>
#include <chrono>
#include <memory>
#include <random>
#include <numeric>

// using this header instead of rdi_buckwalter.hpp to
// avoid circular dependency between the two projects
// it's a dirty workaround.
#include "rdi_dirty_buckwalter.h"
#include "rdi_stl_utils.hpp"
#include "rdi_options.hpp"

#include "rdi_logger.hpp"
#include "rdi_config.hpp"
#include "rdi_multiton.hpp"

using namespace std;
using namespace RDI;


using namespace string_literals;
using namespace chrono;

TEST_CASE("English String","split test")
{
	SECTION("multi tokens")
	{
		string input = " aaa b c  d   s_sa";
		vector<string> correct_result = {"aaa","b","c","d","s_sa"};
		vector<string> expected_result = split(input);
		CHECK(correct_result==expected_result);
	}
	SECTION("one token")
	{
		string input = "aaacds_sa";
		vector<string> correct_result = {"aaacds_sa"};
		vector<string> expected_result = split(input,' ');
		CHECK(correct_result==expected_result);
	}
	SECTION("empty")
	{
		string input = "	";
		vector<string> correct_result = {"	"};
		vector<string> expected_result = split(input,' ');
		CHECK(correct_result==expected_result);
	}
	SECTION("x delimiter")
	{
		string input = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
		vector<string> correct_result = {};
		vector<string> expected_result = split(input,'x');
		CHECK(correct_result==expected_result);
	}
	SECTION("multi tokens","multi delimters")
	{
		string input = " aaa	b c	d	s_sa";
		vector<string> correct_result = {"aaa","b","c","d","s_sa"};
		vector<string> expected_result = split(input, "\t ");
		CHECK(correct_result==expected_result);
	}
}
TEST_CASE("LOGGING")
{
	if (not spdlog::get("rdi_console"))
	{
		RDI_ERROR("WTF");
	}
	RDI_INFO("Name = {}, int = {}, float = {}","soso",3,3.f);
	if (not spdlog::get("rdi_console"))
	{
		RDI_ERROR("WTF");
	}
	RDI_WARN("H{}EL{}LO","HAHA", "YES");
	RDI_FWARN("melog2.txt","I love u daddy{}","thank you very cool");
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FERROR("melog.txt","I love u daddy{}","thank you very cool");
	RDI_WARN("{}",spdlog::get("melog.txt").use_count());
	RDI_WARN("{}",spdlog::get("melog2.txt").use_count());
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_WARN("{}",spdlog::get("melog.txt").use_count());
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FTRACE("melog.txt","I love u daddy{}","thank you very cool");
	RDI_WARN("{}",spdlog::get("rdi_console.txt").use_count());
	RDI_EINFO("THIS IS COOL");

}

// TODO: replace arabic characters with definitions from rdi_ar_letter_definitions.h
TEST_CASE("Arabic String","split test")
{
	SECTION("multi tokens #space")
	{
		wstring input = L" الحب الذى كان بينانا ";
		vector<wstring> correct_result = {L"الحب"
										  ,L"الذى",
										  L"كان"
										  ,L"بينانا"
										 };

        vector<wstring> expected_result = split(input);
		CHECK(correct_result==expected_result);
	}
	SECTION("multi tokens #ح")
	{
		wstring input = L"الحب الذى كان بينانا";
		vector<wstring> correct_result = {L"ال",
										  L"ب الذى كان بينانا"
										 };
		wchar_t d =L'ح';
		vector<wstring> expected_result = split(input,d);
		CHECK(correct_result==expected_result);
	}
	SECTION("one token")
	{
		wstring input = L" الحب الذى كان بينانا ";
		vector<wstring> correct_result = { L" الحب الذى كان بينانا "};
		wchar_t d =L'ش';
		vector<wstring> expected_result = split(input,d);
		CHECK(correct_result==expected_result);
	}
	SECTION("empty")
	{
		wstring input;
		vector<wstring> correct_result = { };
		wchar_t d =L'	';
		vector<wstring> expected_result = split(input,d);
		CHECK(correct_result==expected_result);
	}
	SECTION("س delimiter")
	{
		wstring input = L"سسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسسس";
		vector<wstring> correct_result = {};
		wchar_t d =L'س';
		vector<wstring> expected_result = split(input,d);
		CHECK(correct_result==expected_result);
    }
}


TEST_CASE("intger vector","concat_vectors test")
{
	SECTION("2 full vector")
	{
		vector<int> V1= {1,2,3,4,5};
		vector<int> V2= {6,7,8,9,10};
		vector<int> V3= RDI::concat_vectors(V1,V2);
		vector<int> V3_correct= {1,2,3,4,5,6,7,8,9,10};
		CHECK(V3==V3_correct);
	}
	SECTION("1  empty vector")
	{
		vector<int> V1= {};
		vector<int> V2= {6,7,8,9,10};
		vector<int> V3= RDI::concat_vectors(V1,V2);
		vector<int> V3_correct= {6,7,8,9,10};
		CHECK(V3==V3_correct);
	}
	SECTION("2 empty vector")
	{
		vector<int> V1= {};
		vector<int> V2= {};
		vector<int> V3= RDI::concat_vectors(V1,V2);
		vector<int> V3_correct= {};
		CHECK(V3==V3_correct);
	}
}

TEST_CASE("string vector", "concat_vectors test")
{
	SECTION("2 full vector")
	{
		vector<string> V1= {"hi","my","name","is","islam"};
		vector<string> V2= {"who","are","***","T","F"};
		vector<string> V3= RDI::concat_vectors(V1,V2);
		vector<string> V3_correct= {"hi","my","name","is","islam","who","are","***","T","F"};
		CHECK(V3==V3_correct);
	}
	SECTION("1 empty vector")
	{
		vector<string> V1= {};
		vector<string> V2= {"who","are","***","T","F"};
		vector<string> V3= RDI::concat_vectors(V1,V2);
		vector<string> V3_correct= {"who","are","***","T","F"};
		CHECK(V3==V3_correct);
	}
	SECTION("2 empty vector")
	{
		vector<string> V1= {};
		vector<string> V2= {};
		vector<string> V3= RDI::concat_vectors(V1,V2);
		vector<string> V3_correct= {};
		CHECK(V3==V3_correct);
	}
	SECTION("concaticante a vector of vectors into a vector")
	{
		vector<vector<int>> vofv = {{0, 1}, {8, 2}, {76, 34}, {2, 6}};
		vector<int> v = concat_vectors(vofv);
		CHECK(v == vector<int>({0, 1, 8, 2, 76, 34, 2, 6}));
	}
}

TEST_CASE("remove_all_chars")
{
	SECTION("new lines")
	{
		string x = "\nabc\nd\n\nef\n";
		remove_all_chars(x, '\n');
		CHECK( x == "abcdef");
	}

	SECTION("1 char")
	{
		string y = "abcdef";
		remove_all_chars(y, 'f');
		CHECK( y == "abcde");
	}

}

TEST_CASE("within_container")
{
	SECTION("Found")
	{
		vector<int> V= {1, 2, 3, 4, 5};
		int key = 2;
		bool res  = RDI::within_container(key,V);
		CHECK(res == true);
	}
	SECTION("Not found")
	{
		vector<int> V= {1, 2, 3, 4, 5};
		int key = 10;
		bool res  = RDI::within_container(key,V);
		CHECK(res == false);
	}
	SECTION("String")
	{
		wstring V= L"asdfrrrcchhk03245";
		wchar_t key = L'3';
		bool res  = RDI::within_container(key,V);
		CHECK(res == true);
	}
}
TEST_CASE("remove_spaces")
{
	string str = "islam magdy saieed" ;
	string result_correct = "islammagdysaieed";
	string result = remove_spaces(str);
	CHECK(result_correct == result);
}

TEST_CASE("has_suffix")
{
	string str = "asdfovflkaufiovmarwefujlkjfsaljajvsuffix";
	string suffix = "suffix";
	bool result_correct = true;
	bool result = has_suffix(str, suffix);
	CHECK(result_correct == result);

	wstring ar_str = S_FASLA   +
			S_QUESTION_MARK    +
			S_FASLA_MANKOOTA   +
			S_HAMZA            +
			S_ALEF_MADDA       +
			S_ALEF_HAMZA_ABOVE +
			S_WAW_HAMZA_ABOVE  +
			S_ALEF_HAMZA_BELOW +
			S_YEH_HAMZA_ABOVE  +
			S_ALEF_NO_HAMZA    +
			S_BEH              +
			S_TEH_MARBOOTA     +
			S_TEH              +
			S_THEH             +
			S_JEEM             +
			S_HAH              +
			S_KHAH             +
			S_DAL              +
			S_THAL             +
			S_REH              +
			S_ZAIN;

	wstring ar_suffix = S_KHAH +
			S_DAL              +
			S_REH              +
			S_ZAIN;
	result_correct = false;
	result = has_suffix(ar_str, ar_suffix);
	CHECK(result_correct == result);
}

TEST_CASE("remove_characters_from_the_end")
{
	SECTION("remove more than string size")
	{
		string str = "asdfovflkaufiovmarwefujlkjfsaljajvsuffix";
		size_t n = str.size() + 5;
		CHECK_THROWS(remove_characters_from_the_end(str, n));
	}

	SECTION("normal case")
	{
		string str = "asdfovflkaufiovmarwefujlkjfsaljajvsuffix";
		size_t n = 6;
		string correct_result = "asdfovflkaufiovmarwefujlkjfsaljajv";
		string actual_result = remove_characters_from_the_end(str, n);
		CHECK(correct_result == actual_result);
	}
	SECTION("arabic")
	{
		wstring str = S_FASLA   +
				S_QUESTION_MARK    +
				S_FASLA_MANKOOTA   +
				S_HAMZA            +
				S_ALEF_MADDA       +
				S_ALEF_HAMZA_ABOVE +
				S_WAW_HAMZA_ABOVE  +
				S_ALEF_HAMZA_BELOW +
				S_YEH_HAMZA_ABOVE  +
				S_ALEF_NO_HAMZA    +
				S_BEH              +
				S_TEH_MARBOOTA     +
				S_TEH              +
				S_THEH             +
				S_JEEM             +
				S_HAH              +
				S_KHAH             +
				S_DAL              +
				S_THAL             +
				S_REH              +
				S_ZAIN;
		size_t n = 5;
		wstring correct_result = S_FASLA   +
				S_QUESTION_MARK    +
				S_FASLA_MANKOOTA   +
				S_HAMZA            +
				S_ALEF_MADDA       +
				S_ALEF_HAMZA_ABOVE +
				S_WAW_HAMZA_ABOVE  +
				S_ALEF_HAMZA_BELOW +
				S_YEH_HAMZA_ABOVE  +
				S_ALEF_NO_HAMZA    +
				S_BEH              +
				S_TEH_MARBOOTA     +
				S_TEH              +
				S_THEH             +
				S_JEEM             +
				S_HAH;
		wstring actual_result = remove_characters_from_the_end(str, n);
		CHECK(correct_result == actual_result);
	}
}

TEST_CASE("remove_suffix")
{
	SECTION("does have the suffix")
	{
		string str = "asdfovflkaufiovmarwefujlkjfsaljajvsuffix";
		string suffix = "suffix";
		string result_correct = "asdfovflkaufiovmarwefujlkjfsaljajv";
		string result = remove_suffix(str, suffix);
		CHECK(result_correct == result);
	}

	SECTION("does not have the suffix")
	{
		string str = "asdfovflkaufiovmarwefujlkjfsaljajvsuffi";
		string suffix = "suffix";
		CHECK_THROWS(remove_suffix(str, suffix));
	}
}

TEST_CASE("char to basic_string")
{
	auto c1 =  'c';
	auto c2 = L'c';
	auto c3 = u'c';
	auto c4 = U'c';

	auto nullc1 =  '\0';
	auto nullc2 = L'\0';
	auto nullc3 = u'\0';
	auto nullc4 = U'\0';

	CHECK(char_to_string(c1) ==  "c");
	CHECK(char_to_string(c2) == L"c");
	CHECK(char_to_string(c3) == u"c");
	CHECK(char_to_string(c4) == U"c");

	CHECK(char_to_string(nullc1).empty());
	CHECK(char_to_string(nullc2).empty());
	CHECK(char_to_string(nullc3).empty());
	CHECK(char_to_string(nullc4).empty());
}

TEST_CASE("split to chunks")
{
	SECTION("Random Access Iterator")
	{
		string str = "I hurt myself today, to see if I still feel.";

		vector<string> chunks = split_to_chunks(str, 15);
		CHECK(chunks.size() == 3);
		CHECK(chunks[0] == string("I hurt myself t"));
		CHECK(chunks[1] == string("oday, to see if"));
		CHECK(chunks[2] == string(" I still feel."));
	}

	SECTION("Bidirectional Iterator")
	{
		list<int> l = {4, 2, 5, 6, 7, 8, 9, 2, 34, 23, 65, 12, 76, 23 ,1};

		vector<list<int>> chunks = split_to_chunks(l, 5);

		CHECK(chunks.size() == 3);
		CHECK(chunks[0] == list<int>({4, 2, 5, 6, 7}));
		CHECK(chunks[1] == list<int>({8, 9, 2, 34, 23}));
		CHECK(chunks[2] == list<int>({65, 12, 76, 23 ,1}));
	}

	SECTION("Empty Container")
	{
		vector<float> v;

		vector<vector<float>> chunks = split_to_chunks(v, 4);
		CHECK(chunks.empty());
	}

	SECTION("Huge chunk size")
	{
		vector<string> chunks =
				split_to_chunks(string("you could have it all."),
								3000000);
		CHECK(chunks.size() == 1);
	}
}

TEST_CASE("split to X chunks")
{
	SECTION("Random Access Iterator")
	{
		string str = "I hurt myself today, to see if I still feel.";

		vector<string> chunks = split_to_x_chunks(str, 3);
		CHECK(chunks.size() == 3);
		CHECK(chunks[0] == string("I hurt myself t"));
		CHECK(chunks[1] == string("oday, to see if"));
		CHECK(chunks[2] == string(" I still feel."));
	}

	SECTION("Bidirectional Iterator")
	{
		list<int> l = {4, 2, 5, 6, 7, 8, 9, 2, 34, 23, 65, 12, 76, 23 ,1};

		vector<list<int>> chunks = split_to_x_chunks(l, 3);

		CHECK(chunks.size() == 3);
		CHECK(chunks[0] == list<int>({4, 2, 5, 6, 7}));
		CHECK(chunks[1] == list<int>({8, 9, 2, 34, 23}));
		CHECK(chunks[2] == list<int>({65, 12, 76, 23 ,1}));
	}

	SECTION("Empty Container")
	{
		vector<float> v;

		vector<vector<float>> chunks = split_to_x_chunks(v, 4);
		CHECK(chunks.size() == 4);
	}

	SECTION("Huge chunk size")
	{
		vector<string> chunks =
				split_to_x_chunks(string("you could have it all."),
								3000000);
		CHECK(chunks.size() == 3000000);
	}

	SECTION("divide on zero containers")
	{
		CHECK_THROWS(split_to_x_chunks(string("you could have it all."), 0));
	}
}
TEST_CASE("Throw Unless")
{
	SECTION("RValue Boolean expression")
	{
		CHECK_THROWS(throw_unless(1 == 2,
								  runtime_error("The expression is false")));

		// should return true
		CHECK(throw_unless(1 == 1, runtime_error("The expression is false")));
	}

	SECTION("LValue Boolean expression")
	{
		bool condition = 1 == 2;
		CHECK_THROWS(throw_unless(condition,
								  runtime_error("The expression is false")));

		condition = 1 == 1;
		// should return true
		CHECK(throw_unless(condition, runtime_error("The expression is false")));
	}

	SECTION("Optionals")
	{
		STD_OPTIONAL<string> o = "hello";
		CHECK(throw_unless(o, runtime_error("optional is empty")) ==
						   string("hello"));

		STD_OPTIONAL<string> empty;
		CHECK_THROWS(throw_unless(empty, runtime_error("optional is empty")));
	}

	SECTION("Pointers")
	{
		int* ptr = nullptr;
		CHECK_THROWS(throw_unless(ptr, runtime_error("pointer is null")));

		int x;
		ptr = &x;
		CHECK(throw_unless(ptr, runtime_error("pointer is null")) == ptr);
	}
}

TEST_CASE("collapse")
{
	CHECK(collapse(" abs basasd df d    df      ", ' ') == " abs basasd df d df ");
	CHECK(collapse("rrrrrrrrrrrrrr", 'r') == "r");
	CHECK(collapse("abcddcdcdd", 'd') == "abcdcdcd");
	CHECK(collapse("awwwwwwwwwwwwa", 'w') == "awa");
}

/*TEST_CASE("LOGGING")
{
	if (not spdlog::get("rdi_console"))
	{
		RDI_ERROR("WTF");
	}
	RDI_INFO("Name = {}, int = {}, float = {}","soso",3,3.f);
	if (not spdlog::get("rdi_console"))
	{
		RDI_ERROR("WTF");
	}
	RDI_WARN("H{}EL{}LO","HAHA", "YES");
	RDI_FWARN("melog2.txt","I love u daddy{}","thank you very cool");
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FERROR("melog.txt","I love u daddy{}","thank you very cool");
	RDI_WARN("{}",spdlog::get("melog.txt").use_count());
	RDI_WARN("{}",spdlog::get("melog2.txt").use_count());
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_WARN("{}",spdlog::get("melog.txt").use_count());
	RDI_FWARN("melog.txt","I love u daddy{}","thank you very cool");
	RDI_FTRACE("melog.txt","I love u daddy{}","thank you very cool");
	RDI_WARN("{}",spdlog::get("rdi_console.txt").use_count());


}*/

TEST_CASE("Find all occurrences")
{
	SECTION("std::vector")
	{
		vector<string> v = {
			"Hello",
			"Hi",
			"Hello",
			"Hii",
			"asdf"
		};

		string to_find("Hello");

		vector<size_t> expected = {0, 2};
		vector<size_t> actual = find_all_occurrences(v, to_find);
		CHECK(actual == expected);
	}

	SECTION("std::list")
	{
		list<int> v = {
			3, 4, -2, -2, 3, 5, 6
		};

		int to_find(3);

		vector<size_t> expected = {0, 4};
		vector<size_t> actual = find_all_occurrences(v, to_find);
		CHECK(actual == expected);
	}

	SECTION("std::array")
	{
		array<float, 6> v = {
			3.5f, 1.3461f, 0.231f, 3.253f, 5.287f, 1.3461f
		};

		float to_find = 1.3461f;

		vector<size_t> expected = {1, 5};
		vector<size_t> actual = find_all_occurrences(v, to_find);
		CHECK(actual == expected);
	}

	SECTION("not found")
	{
		array<char, 3> v = {
			'd', '3', 'i'
		};

		char to_find('b');

		vector<size_t> expected = {};
		vector<size_t> actual = find_all_occurrences(v, to_find);
		CHECK(actual == expected);
	}
}

TEST_CASE("Move to X chunks")
{
	SECTION("Random Access Iterator")
	{
		vector<unique_ptr<string>> v;

		for(size_t i = 0; i < 15; i++)
		{
			v.emplace_back(make_unique<string>(to_string(i)));
		}

		vector<vector<unique_ptr<string>>> chunks = move_to_x_chunks(v, 3);

		CHECK(chunks.size() == 3);
		CHECK(*chunks[0][0] == "0");
		CHECK(*chunks[0][1] == "1");
		CHECK(*chunks[0][2] == "2");
		CHECK(*chunks[0][3] == "3");
		CHECK(*chunks[0][4] == "4");
		CHECK(*chunks[1][0] == "5");
		CHECK(*chunks[1][1] == "6");
		CHECK(*chunks[1][2] == "7");
		CHECK(*chunks[1][3] == "8");
		CHECK(*chunks[1][4] == "9");
		CHECK(*chunks[2][0] == "10");
		CHECK(*chunks[2][1] == "11");
		CHECK(*chunks[2][2] == "12");
		CHECK(*chunks[2][3] == "13");
		CHECK(*chunks[2][4] == "14");

	}

	SECTION("Empty Container")
	{
		list<unique_ptr<float>> v;

		vector<list<unique_ptr<float>>> chunks = move_to_x_chunks(v, 4);
		CHECK(chunks.size() == 4);
	}

	SECTION("Huge chunk size")
	{
		vector<unique_ptr<int>> v;
		v.reserve(5);

		for(int i = 0; i < 5; i++)
		{
			v.emplace_back(make_unique<int>(i));
		}

		vector<vector<unique_ptr<int>>> chunks = move_to_x_chunks(v, 3000000);
		CHECK(chunks.size() == 3000000);
	}

	SECTION("divide on zero containers")
	{
		vector<unique_ptr<int>> v;
		CHECK_THROWS(move_to_x_chunks(v, 0));
	}
}

TEST_CASE("insensitve_strcmp")
{
	SECTION("std::string")
	{
		string str1 = "HellO WoRlD!! xD";
		string str2 = "helLO wORlD!! Xd";
		CHECK(insensitive_strcmp(str1, str2));
	}
	SECTION("std::wstring")
	{
		wstring str1 = L"HellO WoRlD!! xD";
		wstring str2 = L"helLO wORlD!! Xd";
		CHECK(insensitive_strcmp(str1, str2));
	}
	SECTION("empty string")
	{
		string str1;
		string str2;
		CHECK(insensitive_strcmp(str1, str2));
	}
	SECTION("not english")
	{
		wstring str1 = S_ALEF_MADDA + S_DAMA;
		wstring str2 = S_ALEF_MADDA + S_DAMA;
		CHECK(insensitive_strcmp(str1, str2));
	}
	SECTION("not equal")
	{
		wstring str1 = S_ALEF_MADDA + S_DAMA;
		wstring str2 = S_ALEF_MADDA + S_BEH;
		CHECK(not insensitive_strcmp(str1, str2));
	}
}

TEST_CASE("Find all occurrences if")
{
	SECTION("std::string")
	{
		string s = "Hello world from RDI";

		vector<size_t> actual = find_all_occurrences_if(s, [](char c)
		{
			return c == 'l';
		});

		vector<size_t> expected {2, 3, 9};

		CHECK(actual == expected);
	}
	SECTION("std::vector")
	{
		vector<int> vec {3, 5, 10, 6, 19, 8, 12, 4, 18, 47, 34, 24};

		vector<size_t> actual = find_all_occurrences_if(vec, [](int i)
		{
			return i % 3 == 0;
		});

		vector<size_t> expected {0, 3, 6, 8, 11};

		CHECK(actual == expected);
	}
	SECTION("std::list")
	{
		list<int> vec {-1, 0, 4, -5, 10, -2};

		vector<size_t> actual = find_all_occurrences_if(vec, [](int i)
		{
			return i > 0;
		});

		vector<size_t> expected {2, 4};

		CHECK(actual == expected);
	}
	SECTION("std::array")
	{
		array<float, 6> v = {
			3.5f, 1.31f, -0.104f, 8.042f, 9.2437f, -4.245f
		};

		vector<size_t> actual = find_all_occurrences_if(v, [](float i)
		{
			return i > 2.0f;
		});

		vector<size_t> expected {0, 3, 4};

		CHECK(actual == expected);
	}
	SECTION("non found")
	{
		vector<string> vec {"hello", "world"};

		vector<size_t> actual = find_all_occurrences_if(vec, [](string s)
		{
			return s.size() > 10;
		});

		CHECK(actual.empty());
	}
	SECTION("empty container")
	{
		vector<int> vec;

		vector<size_t> actual = find_all_occurrences_if(vec, [](int){return true;});

		CHECK(actual.empty());
	}
	SECTION("extra test")
	{
		vector<string> vec1 {"mB", "HI", "AN", "fI", "lI", "mN", "rI", "qI", "ZE"};
		vector<string> vec2 {"mB", "HI", "AN", "mN", "rI", "qI", "ZE"};
		vector<string> vec3 {"mNB", "HI", "AN", "fI", "lI", "mN", "rI", "qI", "ZNE"};
		vector<string> vec4 {"qN"};
		vector<string> vec5 {"keN", "lN;", "wKN", "pNd", "N"};

		vector<size_t> actual_1 = find_all_occurrences_if(vec1, [](string s)
		{
			return std::find(s.begin(), s.end(), 'N') != s.end();
		});
		vector<size_t> actual_2 = find_all_occurrences_if(vec2, [](string s)
		{
			return std::find(s.begin(), s.end(), 'N') != s.end();
		});
		vector<size_t> actual_3 = find_all_occurrences_if(vec3, [](string s)
		{
			return std::find(s.begin(), s.end(), 'N') != s.end();
		});
		vector<size_t> actual_4 = find_all_occurrences_if(vec4, [](string s)
		{
			return std::find(s.begin(), s.end(), 'N') != s.end();
		});
		vector<size_t> actual_5 = find_all_occurrences_if(vec5, [](string s)
		{
			return std::find(s.begin(), s.end(), 'N') != s.end();
		});

		vector<size_t> expected_1 {2, 5};
		vector<size_t> expected_2 {2, 3};
		vector<size_t> expected_3 {0, 2, 5, 8};
		vector<size_t> expected_4 {0};
		vector<size_t> expected_5 {0, 1, 2, 3, 4};

		CHECK(actual_1 == expected_1);
		CHECK(actual_2 == expected_2);
		CHECK(actual_3 == expected_3);
		CHECK(actual_4 == expected_4);
		CHECK(actual_5 == expected_5);
	}
}

TEST_CASE("Config")
{
	string config_path = string(CODE_LOCATION) + "tests/";
	Config& config_map = *RDI::Multiton<Config>::get_instance(config_path);

	CHECK(config_map["Key_1"] == config_map["KEY_1"]);
	CHECK(config_map["keY_2"] == config_map["key_2"]);
	CHECK(config_map["KEY_3"] == config_map["key_3"]);
	CHECK(config_map["keY_4"] == config_map["kEy_4"]);
	CHECK(config_map["KEY_5"] == config_map["keY_5"]);
	CHECK(config_map["Key_6"] == config_map["KEY_6"]);
	CHECK(config_map["keY_7"] == config_map["key_7"]);
	CHECK(config_map["KEY_8"] == config_map["key_8"]);
	CHECK(config_map["keY_9"] == config_map["kEy_9"]);
	CHECK(config_map["KEY_10"] == config_map["keY_10"]);
	CHECK_THROWS(config_map["key_11"] == config_map["key_11"]);

}
