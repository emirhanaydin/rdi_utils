#ifndef RDI_MULTITON_H
#define RDI_MULTITON_H

#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <string>

#include <rdi_config.hpp>

namespace RDI
{

/// The Multiton class is a singleton class (uses get() to get instance)
/// It implements the Multiton Pattern, which is just like the singleton
/// but with a map that carries keys and values for the instances.
/// where the key is an std::string that carries that path to the resource
/// and the value is a shared_ptr to a resource object.
/// A resource could be a model, a configuration file,
/// a graph or anything that is loaded
/// from the disk and is read-only.
/// The Multiton uses get_instance(string key) to get the resource instance
/// The Multiton class itself is using the singleton pattern
/// to make sure that only one multiton is used within the program.
/// The reason we're using shared_ptr instead of unique_ptr
/// is to protect threads that uses the resource object;
/// so that if the deinitialize function is called,
/// the resource is only truly destructed after
/// the last thread using it finishes excecution
template <class RS>
class Multiton
{
    std::map<std::string, std::shared_ptr<RS>> instances;
    std::mutex instances_mutex;
    
    static Multiton<RS>* self;

    Multiton()
    {
    }

public:
	static Multiton<RS>* get()
	{
		if(self)
		{
			return self;
		}

		self = new Multiton<RS>();
		return self;
	}

	template <class... Args>
	static std::shared_ptr<RS>
	get_instance(const std::string& resource_path, Args... args)
	{
		std::lock_guard<std::mutex> instances_lock(get()->instances_mutex);

		if(get()->instances.find(resource_path) != get()->instances.end())
		{
			return get()->instances[resource_path];
		}

		std::shared_ptr<RS> new_instance =
			std::make_shared<RS>(resource_path, args...);

		// insert into the map of instances
		get()->instances[resource_path] = std::move(new_instance);

		return get()->instances[resource_path];
	}

	static void remove_instance(const std::string& resource_path)
	{
		std::lock_guard<std::mutex> instances_lock(get()->instances_mutex);

		get()->instances.erase(resource_path);
	}

	static void destroy()
	{
		if(self)
		{
			delete self;
			self = nullptr;
		}
	}
};

template <class RS>
Multiton<RS>* Multiton<RS>::self = nullptr;

/// @brief Every time a multiton is used you probably use a config file
/// to get the name of certain resource and then use that name to load the
/// resource. If that's your case just call this function with the resource_path
/// (the directory that contains the Config.txt) and the config_key to the
/// specific resource you need.
/// @param resource_path Path to the resource directory which contains
/// the Config.txt
/// @param config_key Key that will be used to retrieve the resource name
/// from the Config.txt
/// @returns A shared_ptr to the resource you need.
template <typename T>
std::shared_ptr<T> get_resource(const std::string& resource_path, const std::string& config_key)
{
    std::shared_ptr<Config> config_ptr = Multiton<Config>::get_instance(resource_path);

    Config& config = *config_ptr;

    return Multiton<T>::get_instance(resource_path + config[config_key]);
}

/// @brief Does the opposite of \ref RDI::get_resource() the above.
template <typename T>
void remove_resource(const std::string& resource_path, const std::string& config_key)
{
    std::shared_ptr<Config> config_ptr = Multiton<Config>::get_instance(resource_path);

    Config& config = *config_ptr;

    Multiton<T>::remove_instance(resource_path + config[config_key]);
}

} // namespace RDI

#endif // RDI_MULTITON_H
