#ifndef RDI_OPTIONS_H
#define RDI_OPTIONS_H

#if __GNUC__ >= 7
#include <any>
#define STD_ANY std::any
#define STD_ANY_CAST std::any_cast
#else
#include <experimental/any>
#define STD_ANY std::experimental::any
#define STD_ANY_CAST std::experimental::any_cast
#endif

#if __GNUC__ >= 7
#include <optional>
#define STD_OPTIONAL std::optional
#else
#include <experimental/optional>
#define STD_OPTIONAL std::experimental::optional
#endif

#include <iostream>
#include <map>
#include <string>

namespace RDI
{

class Options
{
	   std::map<std::string, STD_ANY> opts;

public:
	   Options() {}
	   Options(const Options& other)
	   {
		       this->opts = other.opts;
	   }
	   Options(std::initializer_list<std::pair<const std::string, STD_ANY>>&& _opts)
	           : opts(_opts)
	   {
	   }

	   bool has(const std::string& key)
	   {
		       bool res = opts.find(key) != opts.end();
			   if (!res)
			   {
				       std::cout << "Key (" << key << ") does not exist\n";
			   }
			   return res;
	   }
	   template <typename T = float>
	   STD_OPTIONAL<T> get(const std::string key) const
	   {
		       if (opts.find(key) != opts.end())
			   {
				       return STD_ANY_CAST<T>(opts.at(key));
			   }
			   return {};
	   }
	   void add(const std::string& key, const STD_ANY& val)
	   {
		       opts[key] = val;
	   }
	   void add(std::pair<const std::string, STD_ANY>&& input)
	   {
		       opts[input.first] = input.second;
	   }
};

} // namespace RDI

#endif // RDI_OPTIONS_H
