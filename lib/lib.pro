#-------------------------------------------------
#
# Project created by QtCreator 2017-10-09T18:31:23
#
#-------------------------------------------------

QT       -= core gui

TARGET = rdi_utils
TEMPLATE = lib
CONFIG += staticlib c++1z
QMAKE_CXXFLAGS += -std=c++17

SOURCES += \
    logger.cpp \
    diff.cpp \
    config.cpp

HEADERS += \
    rdi_config.hpp    \
    rdi_multiton.hpp  \
    rdi_options.hpp   \
    rdi_stl_utils.hpp \
    rdi_error_msg.hpp \
    rdi_logger.hpp \
    rdi_diff.hpp \
    ignore_warnings_end.hpp \
    ignore_warnings_start.hpp

INCLUDEPATH += /opt/rdi/include

DISTFILES +=
