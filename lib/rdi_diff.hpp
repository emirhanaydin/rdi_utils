#ifndef RDI_DIFF_HPP
#define RDI_DIFF_HPP

#include <string>
#include <vector>
#include <array>

namespace RDI
{

struct Range
{
	size_t begin;
	size_t end;
};

/// @brief An instruction to do on a vector<string> so that it becomes
/// like another vector<string>
/// Look at \ref RDI::diff() and \ref RDI::patch()
struct Instruction
{
    /// @brief the lines of text to insert or substitute.
	/// In the case of deletion it is left empty. We don't need to know
	/// what we're deleting; the index in
	/// \ref RDI::Instruction::where does the trick.
	std::vector<std::string> what;

	size_t what_idx;

	/// @brief the index(es) in where this change should be applied
	/// If it's a single element then both where.begin and where.end
	/// will be the same number
	Range where;

	enum Type
	{
		Insert,
		Delete,
		Substitute
	};
	Type type;
};

size_t
range_size(Range r);

size_t
range_size(const Instruction& i);

bool
within_range(size_t value, Range r);

size_t
instruction_size(const Instruction& i);

bool
within_instruction_range(size_t value, const Instruction& i);

#ifndef SWIG

/// @brief Will return a set of instructions (a patch)
/// that if applied to vec1. It will become vec2.
/// If you've used the diff command in a UNIX system, it's essentially
/// the same thing.
std::vector<Instruction>
diff(std::vector<std::string> vec1,
	 std::vector<std::string> vec2);

/// @brief applies a set of instructions on a vector<string>.
/// If you have dealt with the diff and patch commands in a UNIX system, it's
/// essentially the same thing.
std::vector<std::string>
patch(const std::vector<std::string>& vec,
      const std::vector<Instruction> instructions);

#endif

} // namespace RDI

#endif // RDI_DIFF_HPP
