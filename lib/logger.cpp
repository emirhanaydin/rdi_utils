#include "rdi_logger.hpp"



namespace RDI
{
std::shared_ptr<spdlog::logger> Logger::_stdout_logger;
std::shared_ptr<spdlog::logger> Logger::_stderr_logger;
std::vector<std::shared_ptr<spdlog::logger>> Logger::_files_loggers;
std::mutex Logger::_file_mt;
std::mutex Logger::_stderr_mt;
std::mutex Logger::_stdout_mt;


void Logger::init_stdout()
{
//		std::lock_guard<std::mutex> lock(_console_mt);
	_stdout_logger = spdlog::stdout_color_mt("rdi_stdout_logger");
	_stdout_logger->set_level(spdlog::level::trace);
	_stdout_logger->set_pattern("[%D][%R] [%l] : %v%$");\

}
void Logger::init_stderr()
{
//		std::lock_guard<std::mutex> lock(_console_mt);
	_stderr_logger = spdlog::stderr_color_mt("rdi_stderr_logger");
	_stderr_logger->set_level(spdlog::level::trace);
	_stderr_logger->set_pattern("[%D][%R] [%l] : %v%$");\

}
void Logger::init_file(const std::string& filename,
					  spdlog::level::level_enum log_level,
					  bool truncate)
{

	auto logger = spdlog::basic_logger_mt(filename, filename, truncate);
	logger->set_level(log_level);
	logger->set_pattern("[%D][%R] [%l] : %v%$");
	_files_loggers.emplace_back(logger);
}

std::shared_ptr<spdlog::logger> &Logger::get_stdout_logger()
{
	std::lock_guard<std::mutex> lock(_stdout_mt);
	auto logger = spdlog::get("rdi_stdout_logger");
	if (not logger)
	{
		init_stdout();
	}
	return _stdout_logger;
}

std::shared_ptr<spdlog::logger> &Logger::get_stderr_logger()
{
	std::lock_guard<std::mutex> lock(_stderr_mt);
	auto logger = spdlog::get("rdi_stderr_logger");
	if (not logger)
	{
		init_stderr();
	}
	return _stderr_logger;
}

std::shared_ptr<spdlog::logger>&
Logger::get_file_logger(const std::string& filename)
{
	std::lock_guard<std::mutex> lock(_file_mt);

	if (not spdlog::get(filename))
	{
		init_file(filename);
	}
	auto it = std::find_if(std::begin(_files_loggers),
						   std::end(_files_loggers),
						   [&filename](const auto& logger) {
							   return logger->name() == filename;
						   });

	return _files_loggers[std::distance(std::begin(_files_loggers), it)];
}
}
