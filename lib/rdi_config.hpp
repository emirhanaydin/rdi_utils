#ifndef RDI_CONFIG_H
#define RDI_CONFIG_H

#include <map>
#include <string>

#include <rdi_stl_utils.hpp>

namespace RDI
{

/// \todo change to be throw error if model not loaded
std::map<std::string , std::string>
parse_config_file(const std::string& filename);

bool
as_bool(const std::string& str);

double
as_double(const std::string& str);

float
as_float(const std::string& str);

int
as_int(const std::string& str);

/// @brief The config class is a readonly class that reads from a file called
/// "Config.txt" within the models_path.
/// You can treat it like a map<string, string>: config["Key"]
/// Config.txt should look something like this:
/// ```
/// key1 = value1
/// key2 = value2
/// ... etc
/// ```
class Config
{
	const std::map<std::string, std::string> config_map;

	std::map<std::string, std::string>
    parse_config(const std::string& models_path);

public:
	Config(const std::string& models_path)
		: config_map(parse_config(models_path))
	{}

	Config(const Config& other) : config_map(other.config_map)
	{}

	~Config()
	{}

    bool has(const std::string& key) const;

    const std::string& operator[](const std::string& key) const;
};

} // namespace RDI

#endif // RDI_CONFIG_H
