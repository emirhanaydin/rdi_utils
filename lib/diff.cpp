#include <vector>
#include <string>
#include <algorithm>
#include <stdexcept>

#include "rdi_diff.hpp"
#include "rdi_stl_utils.hpp"

using namespace std;

namespace RDI
{

namespace
{

/// result will contain the beginning of the common suffix
///
/// In the following example vec1 and vec2 are strings but in reality
/// they're vector<string> but I hope you get the idea
/// example:
/// vec1  = hellomeawroarworld
/// vec2  = hellohsdfworld
/// result = 4
///
/// so if we remove the last 4 chars in each string the result would be:
/// vec1  = hellomeawroar
/// vec2  = hellohsdf
size_t
find_common_suffix(const vector<string>& vec1,
				   const vector<string>& vec2)
{
	long i1 = long(vec1.size());
	long i2 = long(vec2.size());

	while(i1 --> 0 && i2 --> 0)
	{
		if(vec1[size_t(i1)] != vec2[size_t(i2)])
		{
			break;
		}
	}

	// Could be vec2.size() - i2. Makes no difference.
	long output = long(vec1.size()) - i1 - 1;

	return size_t(output);
}

/// result will contain the end of common prefix
/// (the index where vec1 and vec2 start to differ)
///
/// In the following example vec1 and vec2 are strings but in reality
/// they're vector<string> but I hope you get the idea
/// example:
/// vec1  = hellomeawroarworld
/// vec2  = hellohsdfworld
/// result = 5
///
/// so if we take the [0, 5] substring from vec1 and vec2 it would be:
/// vec1  = meowroarworld
/// vec2  = hsdfworld
size_t
find_common_prefix(const vector<string>& vec1,
				   const vector<string>& vec2)
{
	// end of common prefix
	size_t i = 0;

	for(; i < std::min(vec1.size(), vec2.size()); i++)
	{
		if(vec1[i] != vec2[i])
		{
			break;
		}
	}

	return i;
}

template <typename T>
struct Matrix
{
	vector<T> data;
	size_t rows;
	size_t cols;

	T& operator()(size_t x, size_t y);
	const T& operator()(size_t x, size_t y) const;
};

template <typename T>
T& Matrix<T>::operator()(size_t x, size_t y)
{
	size_t i = y * cols + x;

	if(i >= data.size())
	{
		throw out_of_range("bad index: " + to_string(i));
	}

	return data[i];
}

template <typename T>
const T&
Matrix<T>::operator()(size_t x, size_t y) const
{
	size_t i = y * cols + x;

	if(i >= data.size())
	{
		throw out_of_range("bad index: " + to_string(i));
	}

	return data[i];
}

template <typename T>
struct dummy{};

Matrix<int>
make_matrix(dummy<int>, size_t rows, size_t cols)
{
	return Matrix<int>{
		vector<int>(rows * cols, 0),
				rows,
				cols
	};
}

Matrix<char>
make_matrix(dummy<char>, size_t rows, size_t cols)
{
	return Matrix<char>{
		vector<char>(rows * cols, 'e'),
				rows,
				cols
	};
}

template <typename T>
Matrix<T>
make_matrix(size_t rows, size_t cols)
{
	return make_matrix(dummy<T>{}, rows, cols);
}

void
prepend_element(vector<string>& vec)
{
	vec.insert(vec.begin(), "");
}

bool
lies_between(size_t i, Range range)
{
	return i >= range.begin && i <= range.end;
}

vector<string>
vector_from_range(const vector<string>& v, size_t begin, size_t end)
{
	return vector<string>(v.begin() + long(begin),
						  v.begin() + long(end));
}

/// Deletions followed by Insertions in the same range are
/// turned into a single Change instruction
vector<Instruction>
collapse_changes(const vector<Instruction>& original_patch)
{
	if(original_patch.empty())
	{
		return {};
	}

	vector<Instruction> collapsed_patch;
	collapsed_patch.push_back(original_patch.front());
	size_t c_i = 0;

	for(size_t i = 1; i < original_patch.size(); i++)
	{
		const Instruction& o_p = original_patch[i];
		Instruction& c_p = collapsed_patch[c_i];

		// detecting insertion after deletion
		if(!(o_p.type == Instruction::Insert &&
			 c_p.type == Instruction::Delete &&
			 lies_between(o_p.where.begin, c_p.where)))
		{
			// in case there was no insertion after deletion
			collapsed_patch.push_back(o_p);
			c_i++;
			continue;
		}

		// in case there was insertion after deletion

		// the deletion range is equal to the size of things to insert
		// d d d i i i --> s
		if(instruction_size(c_p) == o_p.what.size())
		{
			c_p.type = Instruction::Substitute;
			c_p.where.end = o_p.where.end;
			c_p.what = o_p.what;
			c_p.what_idx = o_p.what_idx;
		}
		// the deletion range is less than the size of things to insert
		// d d i i i --> s i
		else if(instruction_size(c_p) < o_p.what.size())
		{
			c_p.type = Instruction::Substitute;

			vector<string> w1 =
					vector_from_range(o_p.what, 0,
									  instruction_size(c_p));

			vector<string> w2 =
					vector_from_range(o_p.what,
									  instruction_size(c_p),
									  o_p.what.size());

			c_p.what = w1;
			c_p.what_idx = o_p.what_idx;
			c_i++;
			collapsed_patch.push_back(o_p);
			collapsed_patch[c_i].what = w2;
			collapsed_patch[c_i].what_idx += w1.size();
			// insertions are always inserted to a certain place
			// range does not represent the number of elements inserted
			collapsed_patch[c_i].where.end = collapsed_patch[c_i].where.begin;
		}
		// the deletion range is greater than the size of things to insert
		// d d d i i --> d s
		else
		{
			Range& where1 = c_p.where;
			where1.end = where1.begin + (range_size(c_p) - o_p.what.size());

			collapsed_patch.push_back(o_p);
			c_i++;
			Range& where2 = collapsed_patch[c_i].where;

			collapsed_patch[c_i].type = Instruction::Substitute;
			collapsed_patch[c_i].what_idx = o_p.what_idx;
			where2.begin = where1.end + 1;
			where2.end = where2.begin + o_p.what.size() - 1;
		}
	}

	return collapsed_patch;
}

/// If there are multiple instructions of the same type and range
/// we collapse them into a single instruction
vector<Instruction>
collapse_patch(const vector<Instruction>& original_patch)
{
	if(original_patch.empty())
	{
		return {};
	}

	vector<Instruction> collapsed_patch;
	collapsed_patch.push_back(original_patch.front());
	size_t c_i = 0;

	for(size_t i = 1; i < original_patch.size(); i++)
	{
		const Instruction& o_p = original_patch[i];
		Instruction& c_p = collapsed_patch[c_i];

		if(c_p.type == o_p.type &&
		   o_p.type == Instruction::Insert &&
		   o_p.where.begin == c_p.where.begin)
		{
			if(o_p.what.size())
			{
				c_p.what.push_back(o_p.what.front());
			}

			continue;
		}

		if(c_p.type == o_p.type &&
		   o_p.type == Instruction::Delete &&
		   o_p.where.end == c_p.where.end + 1)
		{
			c_p.where.end++;

			if(o_p.what.size())
			{
				c_p.what.push_back(o_p.what.front());
			}

			continue;
		}

		collapsed_patch.push_back(o_p);
		c_i++;
	}

	return collapsed_patch;
}

/// @brief inserts a vector into another
/// @param vec original vector
/// @param elements to be inserted
void
insert_lines(std::vector<std::string>& vec,
			 std::vector<std::string> lines)
{
	vec.insert(vec.end(), lines.begin(), lines.end());
}

struct __Range
{
	long begin;
	long end;
};

struct __Instruction
{
	std::vector<std::string> what;

	long what_idx;

	__Range where;

	Instruction::Type type;
};

vector<__Instruction>
offset_indices(vector<__Instruction> patch, size_t offset)
{
	for(auto& i : patch)
	{
		i.where.begin += offset;
		i.where.end	  += offset;
		i.what_idx    += offset;
	}

	return patch;
}

vector<Instruction>
handle_negative_indices(const vector<__Instruction>& __patch)
{
	vector<Instruction> patch;
	patch.reserve(__patch.size());

	for(const auto& i : __patch)
	{
		patch.push_back(
		{
			i.what,
			size_t(max<long>(i.what_idx, 0)),
			{
				size_t(max<long>(i.where.begin, 0)),
				size_t(max<long>(i.where.end, 0))
			},
			i.type
		});
	}

	return patch;
}

vector<Instruction>
create_insertion_patch(const vector<string>& vec, size_t from)
{
	Instruction output;
	output.what.reserve(vec.size() - from);

	for(size_t i = from; i < vec.size(); i++)
	{
		output.what.push_back(vec[i]);
	}

	output.where = {from-1, from-1};
	output.type = Instruction::Insert;
	output.what_idx = from;

	return {output};
}

vector<Instruction>
create_deletion_patch(const vector<string>& vec, size_t from)
{
	Instruction output;

	output.what = {};
	output.where = {from, vec.size()};
	output.type = Instruction::Delete;
	output.what_idx = from;

	return {output};
}

} // nameless namespace

size_t
range_size(Range r)
{
	return (r.end - r.begin);
}

size_t
range_size(const Instruction& i)
{
	return range_size(i.where);
}

bool
within_range(size_t value, Range r)
{
	return value >= r.begin && value <= r.end;
}

size_t
instruction_size(const Instruction& i)
{
	if(i.type == Instruction::Insert)
	{
		return i.what.size();
	}

	return range_size(i) + 1;
}

bool
within_instruction_range(size_t value, const Instruction& i)
{
	if(i.type == Instruction::Insert)
	{
		Range r{i.where.begin, i.where.begin + instruction_size(i) - 1};
		return within_range(value, r);
	}

	return within_range(value, i.where);
}

std::vector<std::string>
patch(const std::vector<std::string>& vec,
	  const std::vector<Instruction> instructions)
{
	vector<string> output;
	size_t i = 0;

	for(const Instruction& instruction : instructions)
	{
		for(; i <= instruction.where.begin; i++)
		{
			output.push_back(vec[i]);
		}

		if(instruction.type == Instruction::Delete)
		{
			i += (instruction.where.end - instruction.where.begin);
			output.pop_back();
		}
		else if(instruction.type == Instruction::Insert)
		{
			insert_lines(output, instruction.what);
		}
		else if (instruction.type == Instruction::Substitute)
		{
			i += (instruction.where.end - instruction.where.begin);
			output.pop_back();
			insert_lines(output, instruction.what);
		}
	}

	for(; i < vec.size(); i++)
	{
		output.push_back(vec[i]);
	}

	return output;
}

vector<Instruction>
diff(vector<string> vec1, vector<string> vec2)
{
	if(vec1 == vec2)
	{
		return {};
	}

	// we're discarding common prefix and common suffix
	// to reduce the number of elements the diff algorithm
	// will have to work with
	// idea stolen from here: https://neil.fraser.name/writing/diff/
	size_t common_prefix_end = find_common_prefix(vec1, vec2);

	/// handle diff case when 1st is a subset of the other 01350_01_2.wav
	if(common_prefix_end == vec1.size())
	{
		return create_insertion_patch(vec2, vec1.size());
	}

	if(common_prefix_end == vec2.size())
	{
		return create_deletion_patch(vec1, vec2.size());
	}

	vec1 = vector<string>(vec1.begin() + long(common_prefix_end),
						  vec1.end());

	vec2 = vector<string>(vec2.begin() + long(common_prefix_end),
						  vec2.end());

	size_t common_suffix_start = find_common_suffix(vec1, vec2);

	vec1 = vector<string>(vec1.begin(),
						  vec1.end() - long(common_suffix_start));

	vec2 = vector<string>(vec2.begin(),
						  vec2.end() - long(common_suffix_start));

	Matrix<int> mat	   = make_matrix<int> (vec2.size()+1, vec1.size()+1);
	Matrix<char> dir_mat = make_matrix<char>(vec2.size()+1, vec1.size()+1);

	// The way it's done here is by finding the longest common
	// subsequence with a memoization algorithm the time complexity is O(M*N)
	// There are better algorithms but I couldn't be bothered. The input
	// is pretty small anyway and discarding the common prefix/suffix
	// makes it even smaller.

	// prepends an empty string in each vector
	// the matrix was built with vec.size + 1
	// because there needs to be an extra column and row
	// for the algorithm to function correctly
	prepend_element(vec1);
	prepend_element(vec2);

	// Here I'll fill both mat and dir_mat with the longest common subsequence
	// and the direction to go as we backtrack to the beginning.
	// The rules are pretty simple:
	// - if they're equal, increase the lcs and put diagonal as the direction
	// - if they're not, the lcs will be the max between the cell above and
	//   the cell on the left. The direction will be the bigger one. If both
	//   are equal then it doesn't matter which direction we go (up or left).
	for(size_t j = 1; j < mat.rows; j++)
	{
		for(size_t i = 1; i < mat.cols; i++)
		{
			int lcs;
			char direction;

			if(vec1[i] == vec2[j])
			{
				lcs = mat(i-1, j-1) + 1;
				direction = 'd';
			}
			else
			{
				int left = mat(i-1, j);
				int up = mat(i, j-1);
				lcs = max(left, up);
				direction = left > up ? 'l' : 'u';
			}

			mat(i, j) = lcs;
			dir_mat(i, j) = direction;
		}
	}

	vector<__Instruction> __patch;

	long x = long(mat.cols - 1);
	long y = long(mat.rows - 1);

	// Here we backtrack.
	// - If it's a diagonal then they're equal. Nothing to be done.
	// - If it's up then it's an insertion
	// - If it's left then it's a deletion
	// - If it's an empty cell (one of the cells at the edge of the matrix)
	//   * If it's on the left side then it's an insertion
	//   * If it's on the upper side then it's a deletion
	// The index is i-1 because we inserted an extra
	// raw and column to the matrix

	while(!(x == 0 && y == 0))
	{
		char element = dir_mat(size_t(x), size_t(y));

		if(element == 'd')
		{
			x--;
			y--;
		}
		else if(element == 'u')
		{
			__patch.push_back({{vec2[size_t(y)]}, y-1, {x-1, x-1}, Instruction::Insert});
			y--;
		}
		else if(element == 'l')
		{
			__patch.push_back({{}, y-1, {x-1, x-1}, Instruction::Delete});
			x--;
		}
		else if(x == 0)
		{
			__patch.push_back({{vec2[size_t(y)]}, y-1, {x-1, x-1}, Instruction::Insert});
			y--;
		}
		else if(y == 0)
		{
			__patch.push_back({{}, y-1, {x-1, x-1}, Instruction::Delete});
			x--;
		}
	}

	// offseting the indices back to the original vector sizes
	// because we've reduced the size of vectors by excluding
	// common prefixes and suffixes to make the algorithm faster.
	__patch = offset_indices(__patch, common_prefix_end);

	vector<Instruction> patch = handle_negative_indices(__patch);

	// It's backtracking so we reverse to have the instructions in the
	// right order
	std::reverse(patch.begin(), patch.end());

	patch = collapse_patch(patch);
	patch = collapse_changes(patch);



	return patch;
}

} // namespace RDI
