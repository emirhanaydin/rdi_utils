#include <rdi_config.hpp>

#include <vector>
#include <fstream>

using namespace std;

namespace RDI
{

bool
as_bool(const std::string& str)
{
    if(insensitive_strcmp(str, std::string("true")))
    {
        return true;
    }
    if(insensitive_strcmp(str, std::string("1")))
    {
        return true;
    }

    return false;
}

double
as_double(const std::string& str)
{
    return stod(str);
}

float
as_float(const std::string& str)
{
    return stof(str);
}

int
as_int(const std::string& str)
{
    return stoi(str);
}

static vector<string> read_file_lines(const string &filename)
{
    ifstream stream(filename.c_str());
    stringstream ss;
    ss << stream.rdbuf();

    string tmp;
    vector<string> lines;

    while (getline(ss, tmp))
    {
        lines.push_back(tmp);
    }

    return lines;
}

std::map<std::string , std::string>
parse_config_file(const std::string& filename)
{
    using namespace std;
    vector<string> file_content = read_file_lines(filename);
    map<string , string> map_content;

    for(const auto &line : file_content)
    {
        if(line.empty())
        {
            continue;
        }

        vector<string> tokens = split(line, '=');

        throw_unless(tokens.size() == 2,
                     runtime_error(filename + " syntax is not correct"));

        string key = remove_spaces(tokens[0]);
        string value = remove_spaces(tokens[1]);

		std::transform(key.begin(), key.end(), key.begin(), ::tolower);
        map_content[key] = value;
    }

    return map_content;
}

std::map<std::string, std::string>
Config::parse_config(const std::string& models_path)
{
    std::map<std::string, std::string> tmp
            = RDI::parse_config_file(models_path + "Config.txt");
	tmp["models_path"] = models_path;
    return tmp;
}

bool Config::has(const std::string& key) const
{
	string k = key;
	std::transform(k.begin(), k.end(), k.begin(), ::tolower);
	return config_map.find(k) != config_map.end();
}

const std::string& Config::operator[](const std::string& key) const
{
	string k = key;
	std::transform(k.begin(), k.end(), k.begin(), ::tolower);
	if (!has(k))
    {
        throw std::runtime_error("[ERROR] key " + key
                                 + " is not found in Config.txt\n");
    }
	return config_map.at(k);
}

} // namespace RDI
