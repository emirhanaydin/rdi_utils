#ifndef RDI_LOGGER_HPP
#define RDI_LOGGER_HPP


#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/fmt/ostr.h"

namespace RDI
{

/// Simple logging class, uses spdlog logging library
/// clone the repo https://github.com/gabime/spdlog and copy
/// include/spdlog folder to
/// usr/local/include or /usr/include  or /opt/rdi/include
/// you don't need to directly use it, u just use the macros provided
/// to enable logging u need to define RDI_CONSOLE_LOGGING,RDI_FILE_LOGGING
/// before includeing this file
/// usage example:
/// RDI_INFO("Name = {}, int = {}, float = {}","soso",3,3.f); will print
/// [01/10/19][17:34] [info] : Name = soso, int = 3, float = 3
/// RDI_FWARN("melog.txt", "msg"); will create a a file named melog.txt and
/// write [01/10/19][17:34] [warning] : msg in it,
/// calling RDI_FWARN("melog.txt", "msg") again will append to the same file
class Logger
{
public:
	static void init_stderr();
	static void init_stdout();
	static void init_file(const std::string& filename,
						  spdlog::level::level_enum log_level
						  = spdlog::level::trace,
						  bool truncate = false);

	static std::shared_ptr<spdlog::logger>& get_stdout_logger();
	static std::shared_ptr<spdlog::logger>& get_stderr_logger();
	static std::shared_ptr<spdlog::logger>&
	get_file_logger(const std::string& filename);

private:
	static std::shared_ptr<spdlog::logger> _stdout_logger,_stderr_logger;
	static std::vector<std::shared_ptr<spdlog::logger>> _files_loggers;
	static std::mutex _file_mt,_stdout_mt,_stderr_mt;
};




#ifdef RDI_CONSOLE_LOGGING

// console logs
#define RDI_ERROR(...) RDI::Logger::get_stdout_logger()->error(__VA_ARGS__)
#define RDI_WARN(...) RDI::Logger::get_stdout_logger()->warn(__VA_ARGS__)
#define RDI_INFO(...) RDI::Logger::get_stdout_logger()->info(__VA_ARGS__)
#define RDI_TRACE(...) RDI::Logger::get_stdout_logger()->trace(__VA_ARGS__)
#define RDI_DEBUG(...) RDI::Logger::get_stdout_logger()->debug(__VA_ARGS__)
#define RDI_CRITICAL(...) RDI::Logger::get_stdout_logger()->critical(__VA_ARGS__)

#define RDI_EERROR(...) RDI::Logger::get_stderr_logger()->error(__VA_ARGS__)
#define RDI_EWARN(...) RDI::Logger::get_stderr_logger()->warn(__VA_ARGS__)
#define RDI_EINFO(...) RDI::Logger::get_stderr_logger()->info(__VA_ARGS__)
#define RDI_ETRACE(...) RDI::Logger::get_stderr_logger()->trace(__VA_ARGS__)
#define RDI_EDEBUG(...) RDI::Logger::get_stderr_logger()->debug(__VA_ARGS__)
#define RDI_ECRITICAL(...) RDI::Logger::get_stderr_logger()->critical(__VA_ARGS__)

#else
#define RDI_ERROR(...)
#define RDI_WARN(...)
#define RDI_INFO(...)
#define RDI_TRACE(...)
#define RDI_CRITICAL(...)
#define RDI_DEBUG(...)

#define RDI_EERROR(...)
#define RDI_EWARN(...)
#define RDI_EINFO(...)
#define RDI_ETRACE(...)
#define RDI_EDEBUG(...)
#define RDI_ECRITICAL(...)

#endif // CONSOLE_LOGGING

#ifdef RDI_FILE_LOGGING

#define RDI_FERROR(filename, ...) RDI::Logger::get_file_logger(filename)->error(__VA_ARGS__)
#define RDI_FWARN(filename, ...) RDI::Logger::get_file_logger(filename)->warn(__VA_ARGS__)
#define RDI_FINFO(filename, ...) RDI::Logger::get_file_logger(filename)->info(__VA_ARGS__)
#define RDI_FTRACE(filename, ...) RDI::Logger::get_file_logger(filename)->trace(__VA_ARGS__)
#else

#define RDI_FERROR(filename, ...)
#define RDI_FWARN(filename, ...)
#define RDI_FINFO(filename, ...)
#define RDI_FTRACE(filename, ...)
#endif // FILE_LOGGING
// files logs

} // namespace RDI
#endif // RDI_LOGGER_HPP
